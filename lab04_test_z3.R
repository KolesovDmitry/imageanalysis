#Подключаем библиотеку:
library(EBImage)

#Считываем изображение в переменную:
pict = readImage('../Images/cat.jpg')
display(pict, method = "raster")

#Разбиваем изображения по канада
r=pict[,,1]
g=pict[,,2]
b=pict[,,3]

#Для выбора желаемых значений, находим среднее значение 
#и отклонение яркости пикселей в каждом из каналов исходного изображения:

mf_r = mean(r)
mf_r
sf_r = sd(as.vector(r))
sf_r

mf_g = mean(g)
mf_g
sf_g = sd(as.vector(g))
sf_g

mf_b = mean(b)
mf_b
sf_b = sd(as.vector(b))
sf_b

#Задаем желаемые значения для каждого канала:
mg_r = 0.6
sg_r = 0.5

mg_g = 0.7
sg_g = 0.6

mg_b = 0.8
sg_b = 0.7

#Подсчитываем коэффициенты преобразования для каждого канала:
a_r = sg_r/sf_r
d_r = mg_r - mf_r* sg_r/sf_r

a_g = sg_g/sf_g
d_g = mg_g - mf_g* sg_g/sf_g

a_b = sg_b/sf_b
d_b = mg_b - mf_b* sg_b/sf_b

#Проводим коррекцию каждого канала изображения 
r=a_r*r+d_r
g=a_g*r+d_g
b=a_b*r+d_b

#Записываем его в новую переменную:

pict1=pict
pict1[,,1] = r
pict1[,,2] = g
pict1[,,3] = b
display(pict1, method = "raster")

#Адаптивное преобразование
#Создаем массив чисел, используемый для обработки скользящим окном
#(размер матрицы выбираем произвольно, методом подбора): 
f = makeBrush(71)

#Находим среднее значение яркости пикселя внутри скользящего окна:
f=f/sum(f)

#Разбиваем изображения по канада
r=pict[,,1]
g=pict[,,2]
b=pict[,,3]

#Применяем фильтр для каждого канала, который разгладит исходное изображение 
#и поместит в каждый пиксель выборочное среднее окрестных пикселей:
r.mean = filter2(r, f)
display(r.mean, method = "raster")
g.mean = filter2(g, f)
display(g.mean, method = "raster")
b.mean = filter2(b, f)
display(b.mean, method = "raster")

#Рассчетаем выборочную дисперсию для каждого канала. 
#Для этого найдем сначала сумму квадратов разностей 
#между средней яркостью пикселей внутри скользящего окна 
#и яркостью центрального пикселя данного окна:
r.dif = (r - r.mean)^2
display(r.dif, method = "raster")
g.dif = (g - g.mean)^2
display(g.dif, method = "raster")
b.dif = (b - b.mean)^2
display(b.dif, method = "raster")

#Затем при помощи функции filter2 запустим процесс обработки скользящим окном, 
#усредняя полученные на предыдущем шаге разности. 
#В резултате в каждом пикселе каналов r.sd, g.sd и b.sd будет сохранено значение 
#выборочной дисперсии, расчитанной внутри скользящего окна:
r.sd = filter2(r.dif, f)
g.sd = filter2(g.dif, f)
b.sd = filter2(b.dif, f)

#Т.к. яркость пикселя не может быть меньше нуля, произведем нормализацию:
r.sd[r.sd<0]=0
r.sd = sqrt(r.sd)
display(r.sd, method = "raster")

g.sd[g.sd<0]=0
g.sd = sqrt(g.sd)
display(g.sd, method = "raster")

b.sd[b.sd<0]=0
b.sd = sqrt(b.sd)
display(b.sd, method = "raster")

#Произведем коррекцию изображения.
#Задаем желаемые значения:
mg_r = 0.6
sg_r = 0.5

mg_g = 0.7
sg_g = 0.6

mg_b = 0.8
sg_b = 0.7

#Подсчитываем коэффициенты преобразования для каждого канала:
a_r = sg_r/r.sd
d_r = mg_r - r.mean* sg_r/r.sd

a_g = sg_g/g.sd
d_g = mg_g - g.mean* sg_g/g.sd

a_b = sg_b/b.sd
d_b = mg_b - b.mean* sg_b/b.sd

#Проводим коррекцию каждого канала изображения и записываем его в новую переменную:
r = a_r*r + d_r
g = a_g*r + d_g
r = a_b*r + d_b

pict.final = pict
pict.final[,,1] = r
pict.final[,,2] = g
pict.final[,,3] = b
display(pict.final, method = "raster")
